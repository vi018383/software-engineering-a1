# Educational Video Game
## Abstract
## Introduction
## Discussion
The incorporation of gaming into the educational system can be an innovative method into motivating students to learn while at the same time it can keep the subject exciting. Video gaming for educational purposes has been developed in the past. Various studies have been conducted in developing games for educational purposes, it has proven that in some cases it can be an effective method in motivating students into studying and completing further work than they would be doing traditionally [1].

The university has not stated the nature of the game; however, it is mentioned a possibility of a high score table. The use of scoring system would be beneficial as the study conducted by Dr Mark Griffiths stated that “children learn to interact with the score counter”[2] this means that the user is interested in knowing their score, thus it’s a useful feature to include in the game. Through video games, it is also easier to achieve and maintain a person’s undivided attention for long periods of time.[2] Educational video games are also supported by parents, knowing that it would support their children with their studies [2].

Including player character is another useful feature that could be included as it will motivate the student in seeing their character in the game. The use of a timer in the game will give the student a sense of goal and it will increase their excitement [1].

Instant feedback is another useful feature as the user can promptly receive their result, it is a very beneficial feature that should be integrated in the game as the users do not have to wait for long to receive their feedback, as it is pre-programmed in the game.

Initially a pilot project would be useful as data can be collected which can be used in understanding the practicality of the system and it would support in developing the final educational video game.
## Stakeholders to ask about requirements
Software engineering can enable us to develop various software or in this case gaming in order to meet the customer requirements, however before we develop any software (or game), we must understand the customer requirements in detail. 
In order for us to understand about the requirements of the project we must ask the institution to help us gain a good understanding of what to include in the game. The initial step would be to speak with the tutors to specify what should be included in the game. The tutors would have a good understand of the module/subject material that they would like to include in the game and as the game could have different stages, they would be able to advise us what to in include in each stage.

The second class of people to ask are the students. A survey can be conducted from the student and hear their opinion of the game and how they would like the game to be. This can be conducted using google survey or other forms of survey.

## Key points to clarify
In understanding the project requirements, we must clarify the most important points of project such as:
We need to understand who the game users/players are, although this is stated that it is to be designed for university students, however we need to further clarify the subject they are studying, as this would help to understand to design the difficulty of the game as those who study subjects like history, they would prefer a higher difficulty level or those studying mathematics, some of the students may not read vast amount text, they would prefer a lower difficulty level. Knowing their year group can also help us in designing the difficulty level.

The next point to clarify is the subject/reading content to be included. The Atlantic Experience book consist on many chapters and some key historical dates. It must be clarified which chapter is to be tested in what stage and should the students remember the specific dates.
This educational video game is encouraging students to do the set reading; however, we need to understand if there are any deadlines, or the duration in gameplay for each stage.
The most important point to clarify would be student progress; if it is to be monitored then a monitoring system must be included in the game. 

## Constraints
There could be many constraints, however at the early stages the time could be the main constraint as firstly the game is to be developed in time for the university and the time constraint could also be for the game players. If the university advises that the game play is to be timed, then the time constraint must be included in the game. 

If the game is to be monitored , then in that case the legal requirements such as data protection must be considered on how the system is to be monitored.

## Sequence of clarification
## Reference
[1] - Nottingham Trent University. 2002. The educational benefits of video games. [ONLINE] Availableat: http://irep.ntu.ac.uk/id/eprint/15272/1/187769_5405%20Griffiths%20Publisher.pdf. [Accessed 18 October 2020].

[2] - Academia. 2004. More than Just Fun and Games: Assessing the Value of Educational Video Games in the Classroom. [ONLINE] Available at: https://d1wqtxts1xzle7.cloudfront.net/48162290/More_than_just_fun_and_games_Assessing_t20160818-19434-1qjogvl.pdf?1471570871=&response-content-disposition=inline%3B+filename%3DMore_than_just_fun_and_games_assessing_t.pdf&Expires=1603049015&Signature=cdTzGsbQhgK2t1LiquZfm-6BNW2~alT5PJPCitVXzEPJDQPbl6B-Rx6-xi5~FDkCBzbDKKR8qnzzqLooLjS0fdFLrubR43P3ho-9LTaTQ7nIMMSJW-EX33NJPMGLCjaIyB5GH5Q~y2TTHf8WoDPwHj8ttefIH9Ua8Er2tZKk1gmCNB4zHhjBPTVy~r6dwS2BHgtJgMIXyEcfjTAbvAo2dJYsdt-sKD5VPS6TFVThZeh8FCBlFBOHOY5uMQqlcNHBF1F2MYOsgQWVbZsKX4PMvysRxTsEBB2ZxsRhcH7w37-VFWIthygAQYjK1Vtf11uGJxFtRdtTOz~a5rJHKQwN1A__&Key-Pair-Id=APKAJLOHF5GGSLRBV4ZA. [Accessed 18 October 2020].

